using System.ComponentModel.DataAnnotations;

namespace Invento;

public class DatabaseOptions {
    [Required] public required string Host { get; init; }
    public int Port { get; init; } = 5432;
    public string? Database { get; init; }
    [Required] public required string Username { get; init; }
    [Required] public required string Password { get; init; }
}
